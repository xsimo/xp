import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
/**

A public snippet that demonstrate how to turn the byte array returned
by java.security back into a String.

*/
public class Compute_MD5_sum{
    public static String get(String s) throws NoSuchAlgorithmException {
        MessageDigest md5Digest = MessageDigest.getInstance("MD5");
        byte[] bytesArray = md5Digest.digest(s.getBytes());
        String sum = "";
		char current;
		for(byte signedByte : bytesArray) {
			int a;
			if(signedByte <0) {
				a = 256+signedByte;
			}else {
				a = (int) signedByte;
			}
            byte bitFaible = (byte)(a%16);
			byte bitFort = (byte)Math.floor(a/16.0);
			byte[] deux = {bitFort, bitFaible};
			
			for(byte b : deux) {
				if(b<10) {
					current = (char)('0' + b);
					sum += current + "";
				}else {
					current = (char)('a' + b - 10);
					sum += current+"";
				}
			}
		}
		return sum;
    }
    public static void main(String[] args) throws Exception{
        if(args.length==0){
            System.out.println("Usage: java Compute_MD5_sum <string>");
            System.exit(1);
        }
        System.out.println("md5sum(\""+args[0]+"\") = "+ Compute_MD5_sum.get(args[0]));
    }
}